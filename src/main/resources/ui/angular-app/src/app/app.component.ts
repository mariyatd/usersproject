import {Component} from '@angular/core';
import {UserService} from './users/shared/user.service';
import {ConfigService} from './config/config.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  providers: [UserService,
    ConfigService]
})

export class AppComponent {
  title = 'angular-app';
}
