import {Component, OnInit} from '@angular/core';
import {UserService} from '../shared/user.service';
import {User} from '../shared/user.model';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  users;
  selectedUser: User;

  constructor(private userService: UserService) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((data) => {
      this.users = data;
      this.users.forEach((user) => {
        user.birthDate = new Date(user.birthDate);
      });
    });
  }

  onSelect(user: User): void {
    this.selectedUser = user;
  }

  onDelete(user: User): void {
    this.userService.deleteUser(user.userId).subscribe();
    this.users = this.users.filter(item => item !== user);
  }
}
