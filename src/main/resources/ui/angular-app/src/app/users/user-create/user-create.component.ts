import {Component, Input, OnInit} from '@angular/core';
import {User} from '../shared/user.model';
import {UserService} from '../shared/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss']
})

export class UserCreateComponent implements OnInit {
  @Input() user: User = new User('', '', null, '');

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }
}
