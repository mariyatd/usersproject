import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {User} from './user.model';
import {Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {ConfigService} from '../../config/config.service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
};

@Injectable()
export class UserService {
  private usersApiUrl = '/api/users';

  constructor(private http: HttpClient, private configService: ConfigService) {
  }

  getUsers() {
    return this.http.get(this.usersApiUrl);
  }

  addUser(user: User): Observable<User> {
    return this.http.post<User>(this.usersApiUrl, user, httpOptions)
      .pipe(
        catchError(error => this.configService.handleError(error))
      );
  }

  updateUser(user: User): Observable<User> {
    const putUrl = this.usersApiUrl + '/' + user.userId;
    return this.http.put<User>(putUrl, user, httpOptions)
      .pipe(
        catchError(error => this.configService.handleError(error))
      );
  }

  deleteUser(userId: string): Observable<{}> {
    const url = this.usersApiUrl + '/' + userId;
    return this.http.delete(url, httpOptions)
      .pipe(
        catchError(error => this.configService.handleError(error))
      );
  }
}
