export class User {
  userId: string;

  constructor(
    public firstName: string,
    public lastName: string,
    public birthDate: Date,
    public email: string
  ) {
  }
}
