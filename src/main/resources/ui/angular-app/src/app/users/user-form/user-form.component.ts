import {Component, Input, OnInit} from '@angular/core';
import {User} from '../shared/user.model';
import {UserService} from '../shared/user.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})

export class UserFormComponent implements OnInit {
  @Input() user: User = new User('', '', null, '');
  @Input() submitButtonText;
  @Input() alertText;

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.user.userId) {
      this.userService.updateUser(this.user).subscribe();
    } else {
      this.userService.addUser(this.user).subscribe();
    }

    alert(this.alertText);
    this.router.navigate(['/users']);
  }
}
