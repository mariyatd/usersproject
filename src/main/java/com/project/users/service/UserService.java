package com.project.users.service;

import com.project.users.UserRepository;
import com.project.users.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    private UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public void saveOrUpdateUser(User User) {
        userRepository.save(User);
    }

    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }
}
