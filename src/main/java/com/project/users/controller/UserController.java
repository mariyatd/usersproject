package com.project.users.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.users.model.User;
import com.project.users.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    private UserService userService;

    private ObjectMapper mapper = new ObjectMapper();


    @GetMapping
    public List<User> getUsers() {
        return userService.findAll();
    }
    // TODO pagination
    // TODO validation

    @PostMapping
    public ResponseEntity<?> createUser(@RequestBody User user) {
        //TODO make headers everywhere
        HttpHeaders header = new HttpHeaders();
        header.set("Accept", MediaType.ALL_VALUE);

        userService.saveOrUpdateUser(user);
        return ResponseEntity.ok()
                .headers(header)
                .body("User added");
    }

    @PutMapping(value = "/{userId}")
    public ResponseEntity<?> updateUser(@RequestBody User user) {
        HttpHeaders header = new HttpHeaders();
        header.set("Accept", MediaType.ALL_VALUE);

        userService.saveOrUpdateUser(user);
        return ResponseEntity.ok()
                .headers(header)
                .body("User " + user.getUserId() + " updated");
    }

    @DeleteMapping(value = "/{userId}")
    public ResponseEntity<?> deleteStudent(@PathVariable String userId) {
        userService.deleteUser(userId);
        return new ResponseEntity("User with id " + userId + " deleted successfully", HttpStatus.OK);
    }
}
